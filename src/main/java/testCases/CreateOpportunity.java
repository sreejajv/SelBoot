package testCases;

import java.util.ArrayList;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseClass.BaseClass;
import pages.LoginPage;

public class CreateOpportunity extends BaseClass{

	@BeforeTest
	public void setExcelSheet() {
	//location of the excel file for this test case
	excelSheetPath = "./data/CreateOpportunity.xlsx";
	//the column number which has date entered, please note column number starts from zero.
	dateInExcel = new ArrayList<Integer>();
	dateInExcel.add(3);
	}
	
	@Test(dataProvider="fetchData")
	public void createOpportunity(String uName, String password, String name, String date) {
		System.out.println(uName+":"+password+":"+name+date);
		new LoginPage(driver)
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickWaffle() 
		.clickViewAll() 
		.clickSales() 
		.clickArrowNearOpportunity()
		.clickNewOpportunity() 
		.enterName(name) 
		.enterDate(date)
		.selectStageAsNeedAnalysis() 
		.clickSave() 
		.verifyOpprCreated()
		.assertAll();
	}
	
}
