package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseClass.BaseClass;
import pages.LoginPage;

public class DeleteAccounts extends BaseClass {

	@BeforeTest
	public void setExcelSheet() {
	//location of the excel file for this test case
	excelSheetPath = "./data/DeleteAccounts.xlsx";
	}
	
	@Test(dataProvider="fetchData")
	public void editAccounts(String uName, String password, String searchName) {
		new LoginPage(driver)
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickWaffle()
		.clickViewAll()
		.clickSales()
		.clickAccountsTab()
		.searchByName(searchName)
		.selectFromSearchResults(searchName)
		.deleteAccounts()
		.confirmDelete()
		.verifyAccountDeleted()
		.assertAll();
	}
	
}
