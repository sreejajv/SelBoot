package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.asserts.SoftAssert;

import baseClass.BaseClass;

public class AccountsPage extends BaseClass{
	SoftAssert sassert = new SoftAssert() ;
	
	public AccountsPage(ChromeDriver driver) {
		this.driver = driver;
	}
	
	public AccountsPage clickNew() {
		driver.findElement(By.xpath("//div[text()='New']")).click();
		return this;
	}
	
	public AccountsPage enterAccountName(String AccName) {
		System.out.println("Hi");
		driver.findElement(By.xpath("//label[text()='Account Name']/following::div[1]/input")).sendKeys(AccName);
		return this;
	}
	
	public AccountsPage selectOwnerShipAsPublic() {
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath("//label[text()='Ownership']/following::div[1]//input")));
		driver.findElement(By.xpath("//span[text()='Public']")).click();
		return this;
	}
	
	public AccountsPage clickSave() {
		driver.findElement(By.xpath("//button[text()='Save']")).click();
		return this;
	}
	
	public AccountsPage verifyAccountCreated() {
		WebElement msg = driver.findElement(By.xpath("//span[text()[contains(.,'was created.')]]"));
		sassert.assertTrue(msg.isDisplayed());
		return this;
	}
	
	public AccountsPage assertAll() {
		sassert.assertAll();
		System.out.println("Hi");
		return this;
	}
	
	public AccountsPage searchByName(String searchName) {
		driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(searchName+Keys.RETURN);
		driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Keys.ENTER);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public AccountsPage selectFromSearchResults(String searchName) {
		driver.executeScript("arguments[0].scrollIntoView()", driver.findElement(By.xpath("//a[text()='"+searchName+"']/ancestor::tr/td[6]//a")));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
					e.printStackTrace();
		}
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='"+searchName+"']/ancestor::tr/td[6]//a")));
		return this;
	}
	
	public AccountsPage editAccount() {
		driver.findElement(By.xpath("//div[text()='Edit']/parent::a")).click();
		return this;
	}
	
	public AccountsPage selectTypeAsTechnologyPartner() {
		driver.findElement(By.xpath("//label[text()='Type']/following::div[1]//input")).click();
		driver.executeScript("arguments[0].scrollIntoView()", driver.findElement(By.xpath("//span[text()='Technology Partner']")));
		driver.findElement(By.xpath("//span[text()='Technology Partner']")).click();
		return this;
	}
	
	public AccountsPage selectIndustryAsHealthCare() {
		driver.findElement(By.xpath("//label[text()='Industry']/following::div[1]//input")).click();
		driver.executeScript("arguments[0].scrollIntoView()",driver.findElement(By.xpath("//span[text()='Healthcare']")));
		driver.findElement(By.xpath("//span[text()='Healthcare']")).click();
		return this;
	}
	 
	public AccountsPage enterBillingStreet(String billingStreet) {
		driver.findElement(By.xpath("//label[text()='Billing Street']/following::div/textarea")).sendKeys(billingStreet);
		return this;
	}
	
	public AccountsPage enterShippingStreet(String shippingStreet) {
		driver.findElement(By.xpath("//label[text()='Shipping Street']/following::div/textarea")).sendKeys(shippingStreet);
		return this;
	}
	
	public AccountsPage selectCustomerPriorityAsLow() {
		driver.executeScript("arguments[0].scrollIntoView()",driver.findElement(By.xpath("//label[text()='Customer Priority']/following::div[1]//input")));
		driver.findElement(By.xpath("//label[text()='Customer Priority']/following::div[1]//input")).click();
		driver.findElement(By.xpath("//span[text()='Low']")).click();
		return this;
	}
	
	public AccountsPage selectSLAAsSilver() {
		driver.findElement(By.xpath("//label[text()='SLA']/following::div[1]//input")).click();
		driver.findElement(By.xpath("//span[text()='Silver']")).click();
		return this;
	}
	
	public AccountsPage selectActiveAsNo() {
		driver.findElement(By.xpath("//label[text()='Active']/following::div[1]//input")).click();
		driver.findElement(By.xpath("//span[text()='No']")).click();
		return this;
	}
	
	public AccountsPage enterPhoneNumber(String phoneNumber) {
		driver.findElement(By.xpath("//label[text()='Phone']/following-sibling::div/input")).sendKeys(phoneNumber);
		return this;
	}
	
	public AccountsPage selectUpsellOpportunityAsNo() {
		driver.executeScript("arguments[0].scrollIntoView()",driver.findElement(By.xpath("//label[text()='Upsell Opportunity']/following-sibling::div//input")));
		driver.findElement(By.xpath("//label[text()='Upsell Opportunity']/following-sibling::div//input")).click();
		driver.findElement(By.xpath("//label[text()='Upsell Opportunity']/following-sibling::div//span[text()='No']")).click();
		return this;
	}
	
	
	public AccountsPage verifyPhoneNumber(String phoneNumber) {
		String phoneNumberFromApp = driver.findElement(By.xpath("//a[text()='Sreeja']/ancestor::tr/td[4]/span/span")).getText();
		System.out.println("Phone"+phoneNumber);
		sassert.assertEquals(phoneNumber, phoneNumberFromApp);
		return this;
	}
	
	public AccountsPage deleteAccounts() {
		driver.findElement(By.xpath("//div[text()='Delete']/parent::a")).click();
		return this;
	}
	
	public AccountsPage confirmDelete() {
		driver.findElement(By.xpath("//span[text()='Delete']/parent::button")).click();
		return this;
	}
	
	public AccountsPage verifyAccountDeleted() {
		WebElement msg = driver.findElement(By.xpath("//span[text()[contains(.,'was deleted.')]]"));
		sassert.assertTrue(msg.isDisplayed());
		return this;
	}
}
