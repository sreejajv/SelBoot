package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.asserts.SoftAssert;

import baseClass.BaseClass;

public class OpportunityPage extends BaseClass{
	SoftAssert sassert = new SoftAssert();
	
	public OpportunityPage(ChromeDriver driver){
		this.driver = driver;
	}
	
	public OpportunityPage enterName(String name) {
		driver.findElement(By.xpath("//input[@name='Name']")).clear();
		driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(name);
		return this;
	}
	
	public OpportunityPage enterDate(String date) {
		driver.findElement(By.xpath("//input[@name='CloseDate']")).sendKeys(date);
		return this;
	}
	
	public OpportunityPage selectStageAsNeedAnalysis() {
		WebElement dropopp = driver.findElement(By.xpath("//label[text()='Stage']/following::div[1]//input[@class='slds-input slds-combobox__input']"));
		driver.executeScript("arguments[0].click();", dropopp);
		driver.findElement(By.xpath("//span[text()='Needs Analysis']")).click();
		return this;
	}
	
	public OpportunityPage clickSave() {
		driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
		return this;
	}
	
	public OpportunityPage verifyOpprCreated() {
		WebElement msg = driver.findElement(By.xpath("//span[text()[contains(.,'was created.')]]"));
		sassert.assertTrue(msg.isDisplayed());
		return this;
	}
	
	public OpportunityPage assertAll() {
		sassert.assertAll();
		return this;
	}
}
